# Viandate

Viandante is a traveler platform that allows users to discover new places.

It is built using **React** and relies upon **GraphQL** to retrieve data.

> _Viandante is the italian word for "wanderer"_

![](screenshots/viandante_screenshot.png)

## Development

This project uses yarn workspaces, each `package` contains specific information
on how to use it.

## Packages

You can launch package-specific scripts by using
`yarn workspace @viandante/<package name> <script>`.

- `@viandante/platform`
  - Root package
- `@viandante/react-app`
  - React PWA

## Codegen

Since this whole project relies heavily on GraphQL, a common
`graphql-code-generator` config is used.

Every time a `.graphql` file is updated you should launch:

```
yarn codegen
```

This will generate type definintions and operations-related code.

## Docker

At the moment a single Dockerfile, for building and serving the react PWA, is
provided.

**Don't forget to configure the
[environment variables](packages/react-app/README.md#environment-variables) first!**

You can build the image by running:

```
  docker build -t viandante-react-app -f packages/react-app/Dockerfile .
```

You can then run a container by launching this command:

```
  docker run -p 8080:80 viandante-react-app
```

And it's done! You can now visit the PWA on
[http://localhost:8080](http://localhost:8080)
