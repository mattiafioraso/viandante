# WORK SUMMARY

- [x] Read API documentation and try it
- [x] Design some UI on Sketch
- [x] Setup repository using yarn workspaces (in order to accomodate possible
      future packages)
- [x] Setup react app with create-react-app
- [x] Setup linter and formatter
- [x] Setup client router
- [x] Setup Apollo client
- [x] Setup map component
- [x] Setup basic styling and components
- [x] Setup data visualization components
- [x] Setup queries
- [x] Setup theming
- [x] Setup PWA capabilities
- [x] Setup firebase for test and demo purposes
- [x] Setup Docker
- [x] Integrate documentation
- [x] Implement tests
