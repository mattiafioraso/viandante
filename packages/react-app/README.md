This project was bootstrapped with
[Create React App](https://github.com/facebook/create-react-app).

## Environment variables

To setup the environment you should copy the `.env` file into `.env.local`.
You'll need to setup accounts for the [Everbase](https://www.everbase.co/) API
and the [Mapbox](https://www.mapbox.com/) API.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br /> Open
[http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br /> You will also see any lint errors
in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br /> It correctly bundles
React in production mode and optimizes the build for the best performance.

### `yarn deploy`

Deploys a production build of the app on
[Firebase](https://firebase.google.com/).

Note that you first need to create a Firebase project and login
