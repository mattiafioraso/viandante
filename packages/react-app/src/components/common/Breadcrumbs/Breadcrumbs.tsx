import styled from 'styled-components';

export default styled.nav`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  margin-bottom: 48px;

  a {
    color: ${({ theme }) => theme.palette.text.secondary};
    text-decoration: none;

    &:hover,
    &:focus {
      text-decoration: underline;
    }
  }

  > *:not(:last-child) {
    position: relative;
    margin-right: 32px;

    &::after {
      content: '>';
      position: absolute;
      display: inline-block;
      width: 32px;
      text-align: center;
      right: -32px;
      color: ${({ theme }) => theme.palette.divider.primary};
    }
  }
`;
