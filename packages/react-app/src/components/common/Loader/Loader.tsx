import { FunctionComponent } from 'react';
import React from 'react';
import styled from 'styled-components';

const LoaderContainer = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const LoaderSvg = styled.svg`
  position: relative;
  animation: loader-rotation 1s linear infinite;

  > circle {
    fill: none;
    stroke-width: 4;
    stroke: ${({ theme }) => theme.palette.main.primary};
    stroke-dasharray: 48;
    stroke-linecap: round;
  }

  @keyframes loader-rotation {
    from {
      transform: rotateZ(0deg);
    }
    to {
      transform: rotateZ(360deg);
    }
  }
`;

const Loader: FunctionComponent = () => (
  <LoaderContainer>
    <LoaderSvg height={48} width={48}>
      <circle cx={24} cy={24} r={22} />
    </LoaderSvg>
  </LoaderContainer>
);

export default Loader;
