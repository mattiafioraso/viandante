import { HTMLAttributes, ReactNode } from 'react';

import { LinkProps } from 'react-router-dom';

export type PlaceCardProps = HTMLAttributes<HTMLDivElement> & {
  readonly title?: ReactNode;
  readonly to?: LinkProps['to'];
};
