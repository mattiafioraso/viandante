import React, { FunctionComponent } from 'react';

import { Link } from 'react-router-dom';

import styled from 'styled-components';

import { PlaceCardProps } from './PlaceCard.model';

const PlaceCard: FunctionComponent<PlaceCardProps> = ({
  title,
  to,
  ...props
}) => (
  <div {...props}>
    <span>{title}</span>
    {to && <Link to={to}>Explore</Link>}
  </div>
);

export default styled(PlaceCard)`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  padding: 16px 16px 16px 24px;
  border-radius: ${({ theme }) => theme.borderRadius};
  border: 1px solid ${({ theme }) => theme.palette.divider.primary};
  background-color: ${({ theme }) => theme.palette.background.primary};

  > span {
    font-size: 1.25rem;
    font-weight: 500;
    padding-right: 24px;
  }

  > a {
    display: inline-block;
    padding: 0 16px;
    line-height: 2.25rem;
    border-radius: inherit;
    text-decoration: none;
    font-weight: 500;
    color: ${({ theme }) => theme.palette.main.contrast};
    background-color: ${({ theme }) => theme.palette.main.primary};

    &:hover,
    &:focus {
      background-color: ${({ theme }) => theme.palette.main.secondary};
    }
  }
`;
