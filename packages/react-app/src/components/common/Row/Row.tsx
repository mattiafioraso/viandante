import styled from 'styled-components';

import { RowProps } from './Row.model';

export default styled.div<RowProps>`
  display: flex;
  flex-wrap: wrap;

  &:not(:last-child) {
    margin-bottom: ${({ disableMargin }) => (disableMargin ? '0' : '24px')};
  }

  & > * {
    margin-bottom: 16px;

    &:not(:last-child) {
      margin-right: 16px;
    }
  }
`;
