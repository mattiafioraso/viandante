import { HTMLAttributes, FunctionComponent } from 'react';

export type SearchableListProps<
  T extends Record<string, unknown>
> = HTMLAttributes<HTMLDivElement> & {
  readonly items: readonly T[];
  readonly searchKey: keyof T;
  readonly idKey: keyof T;
  readonly renderItem: (item: T) => ReturnType<FunctionComponent>;
};

export type SearchableListComponent = <T extends Record<string, unknown>>(
  props: SearchableListProps<T>,
) => ReturnType<FunctionComponent>;
