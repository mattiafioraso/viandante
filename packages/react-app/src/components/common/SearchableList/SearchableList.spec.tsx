import React from 'react';

import { render, screen, fireEvent } from '@testing-library/react';

import { ThemeProvider } from 'styled-components';

import { lightTheme } from '../../../config/theme/theme';

import SearchableList from './SearchableList';

describe('SearchableList', () => {
  const itemsFixture = [
    { id: 'test-1', value: 'The first value' },
    { id: 'test-2', value: 'A second value' },
    { id: 'test-3', value: 'Go on with the third!' },
    { id: 'test-4', value: 'The last value' },
  ];

  beforeEach(() => {
    render(
      <ThemeProvider theme={lightTheme}>
        <SearchableList
          items={itemsFixture}
          idKey="id"
          searchKey="value"
          renderItem={({ value }) => <span>{value}</span>}
        />
      </ThemeProvider>,
    );
  });

  it('should show all options if no search is performed', () => {
    const itemsCount = screen.queryAllByTestId('SearchableList/list-item')
      .length;

    expect(itemsCount).toEqual(4);
  });

  it('should match entire words', () => {
    const searchInput = screen.getByTestId('SearchableList/field');

    fireEvent.change(searchInput, { target: { value: 'the' } });

    const itemsCount = screen.queryAllByTestId('SearchableList/list-item')
      .length;

    expect(itemsCount).toEqual(3);
  });

  it('should match substrings', () => {
    const searchInput = screen.getByTestId('SearchableList/field');

    fireEvent.change(searchInput, { target: { value: 'on' } });

    const itemsCount = screen.queryAllByTestId('SearchableList/list-item')
      .length;

    expect(itemsCount).toEqual(2);
  });

  it('should trim spaces', () => {
    const searchInput = screen.getByTestId('SearchableList/field');

    fireEvent.change(searchInput, { target: { value: '  value ' } });

    const itemsCount = screen.queryAllByTestId('SearchableList/list-item')
      .length;

    expect(itemsCount).toEqual(3);
  });

  it('should not show list items if there is no match', () => {
    const searchInput = screen.getByTestId('SearchableList/field');

    fireEvent.change(searchInput, { target: { value: 'cat' } });

    const itemsCount = screen.queryAllByTestId('SearchableList/list-item')
      .length;

    expect(itemsCount).toEqual(0);
  });
});
