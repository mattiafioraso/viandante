import React, { useState, useCallback, ChangeEventHandler } from 'react';

import styled from 'styled-components';

import { transparentize } from 'polished';

import { SearchableListComponent } from './SearchableList.model';

const SearchableList: SearchableListComponent = ({
  items,
  idKey,
  searchKey,
  renderItem,
  ...props
}) => {
  type ItemType = typeof items[number];

  const [searchValue, setSearchValue] = useState('');

  const handleChange = useCallback<ChangeEventHandler<HTMLInputElement>>(
    ({ target: { value } }) => {
      setSearchValue(value);
    },
    [],
  );

  const matchesSearch = useCallback(
    (item: ItemType) => {
      const formattedItemValue = `${item[searchKey]}`
        .trim()
        .toLocaleLowerCase();
      const formattedSearchValue = searchValue.trim().toLocaleLowerCase();

      return formattedItemValue.includes(formattedSearchValue);
    },
    [searchKey, searchValue],
  );

  const renderListItem = useCallback(
    (item: typeof items[number]) => (
      <li data-testid="SearchableList/list-item" key={`${item[idKey]}`}>
        {renderItem(item)}
      </li>
    ),
    [items, idKey, renderItem],
  );

  return (
    <div {...props}>
      <header>
        <input
          data-testid="SearchableList/field"
          placeholder="Search"
          value={searchValue}
          onChange={handleChange}
        />
      </header>
      <ul>{items.filter(matchesSearch).map(renderListItem)}</ul>
    </div>
  );
};

export default styled(SearchableList)`
  position: relative;

  > header {
    position: sticky;
    top: 0;
    padding: 24px 0;
    background: linear-gradient(
      180deg,
      ${({ theme }) => theme.palette.background.primary} 80%,
      ${({ theme }) => transparentize(1)(theme.palette.background.primary)} 100%
    );

    > input {
      appearance: none;
      width: 100%;
      box-sizing: border-box;
      line-height: 3.5rem;
      padding: 0 24px;
      font-size: 1rem;
      font-weight: 500;
      border-radius: ${({ theme }) => theme.borderRadius};
      /* border: 1px solid ${({ theme }) => theme.palette.divider.primary}; */
      border: 0;
      background-color: ${({ theme }) => theme.palette.background.secondary};
      color: ${({ theme }) => theme.palette.text.primary};

      &::placeholder {
        color: ${({ theme }) => theme.palette.text.secondary};
      }
    }
  }

  > ul {
    list-style-type: none;
    padding: 0;
  }
` as SearchableListComponent; // NOTE: This cast is needed in order to infer items type.
