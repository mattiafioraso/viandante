export type BadgeProps = {
  readonly backgroundColor?: string;
  readonly color?: string;
};
