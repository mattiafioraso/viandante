import styled from 'styled-components';

import { BadgeProps } from './Badge.model';

export default styled.div<BadgeProps>`
  display: flex;
  width: 72px;
  height: 72px;
  border-radius: ${({ theme }) => theme.borderRadius};
  justify-content: center;
  align-items: center;
  font-size: 2rem;
  font-weight: 500;
  color: ${({ theme, color }) => color ?? theme.palette.main.contrast};
  background-color: ${({ theme, backgroundColor }) =>
    backgroundColor ?? theme.palette.main.primary};
`;
