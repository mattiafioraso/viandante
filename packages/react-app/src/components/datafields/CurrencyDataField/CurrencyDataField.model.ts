export type CurrencyDataFieldProps = {
  readonly main?: string;
  readonly unitSymbol?: string;
  readonly other?: readonly string[];
};
