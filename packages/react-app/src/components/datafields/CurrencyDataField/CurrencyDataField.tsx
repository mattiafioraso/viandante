import React, { FunctionComponent, Fragment } from 'react';

import { CurrencyDataFieldProps } from './CurrencyDataField.model';

import Row from '../../common/Row/Row';
import Badge from '../Badge/Badge';
import DataField from '../DataField/DataField';

const CurrencyDataField: FunctionComponent<CurrencyDataFieldProps> = ({
  main,
  unitSymbol = '?',
  other = [],
}) => (
  <Row>
    <Badge backgroundColor="#ffdd91" color="black">
      {unitSymbol}
    </Badge>
    <DataField
      label="Currencies"
      info={
        other.length > 0 && (
          <>
            Other currencies are{' '}
            {other.map((lang, index) => (
              <Fragment key={lang}>
                <strong>{lang}</strong>
                {index < other.length - 1 && `, `}
              </Fragment>
            ))}
          </>
        )
      }
    >
      {main}
    </DataField>
  </Row>
);

export default CurrencyDataField;
