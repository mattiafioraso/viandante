import { ReactNode } from 'react';

export type DataFieldProps = {
  readonly label?: ReactNode;
  readonly info?: ReactNode;
};
