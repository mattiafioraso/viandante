import React, { FunctionComponent } from 'react';

import styled from 'styled-components';

import { DataFieldProps } from './DataField.model';

const Container = styled.div`
  width: 272px;
  flex: 1 1 auto;

  > * {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;

const Label = styled.div`
  height: 1.5rem;
  font-size: 1rem;
  line-height: 1.5rem;
`;

const Content = styled.div`
  height: 3rem;
  font-size: 2rem;
  line-height: 3rem;
  font-weight: 500;
`;

const Info = styled.div`
  height: 1rem;
  font-size: 0.75rem;
  line-height: 1rem;
`;

const DataField: FunctionComponent<DataFieldProps> = ({
  children,
  label,
  info,
}) => (
  <Container>
    <Label>{label}</Label>
    <Content>{children}</Content>
    <Info>{info}</Info>
  </Container>
);

export default DataField;
