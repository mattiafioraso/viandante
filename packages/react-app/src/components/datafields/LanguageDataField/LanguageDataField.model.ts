export type LanguageDataFieldProps = {
  readonly main?: string;
  readonly other?: readonly string[];
};
