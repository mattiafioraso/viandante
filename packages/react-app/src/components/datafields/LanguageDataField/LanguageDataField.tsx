import React, { FunctionComponent, Fragment } from 'react';

import { LanguageDataFieldProps } from './LanguageDataField.model';

import Row from '../../common/Row/Row';
import Badge from '../Badge/Badge';
import DataField from '../DataField/DataField';

const LanguageDataField: FunctionComponent<LanguageDataFieldProps> = ({
  main,
  other = [],
}) => (
  <Row>
    <Badge backgroundColor="#a4d2ec" color="black">
      {main ? other.length + 1 : '?'}
    </Badge>
    <DataField
      label="Languages"
      info={
        other.length > 0 && (
          <>
            Other languages are{' '}
            {other.map((lang, index) => (
              <Fragment key={lang}>
                <strong>{lang}</strong>
                {index < other.length - 1 && `, `}
              </Fragment>
            ))}
          </>
        )
      }
    >
      {main}
    </DataField>
  </Row>
);

export default LanguageDataField;
