export type PercentagePieChartProps = {
  readonly size?: number;
  readonly value?: number;
};
