import React, { FunctionComponent, SVGAttributes } from 'react';

import { PercentagePieChartProps } from './PercentagePieChart.model';
import styled from 'styled-components';

const BackgroundCircle = styled.circle`
  fill: ${({ theme }) => theme.palette.background.secondary};
`;

const ForegroundCircle = styled.circle`
  stroke: ${({ theme }) => theme.palette.main.primary};
  fill: none;
  transform: rotateZ(-90deg);
  transform-origin: center;
`;

const PercentagePieChart: FunctionComponent<PercentagePieChartProps> = ({
  size = 72,
  value = 0,
}) => {
  const sanitizedValue = Math.max(Math.min(value || 0, 1), 0);

  const radius = size / 2;

  const commonCircleProps: SVGAttributes<SVGCircleElement> = {
    cx: radius,
    cy: radius,
  };

  const circumference = radius * Math.PI;

  const strokeLength = circumference * sanitizedValue;

  return (
    <svg height={size} width={size}>
      <BackgroundCircle {...commonCircleProps} r={radius} />
      <ForegroundCircle
        {...commonCircleProps}
        r={radius / 2}
        strokeWidth={radius}
        strokeDasharray={`${strokeLength} ${circumference - strokeLength}`}
      />
    </svg>
  );
};

export default PercentagePieChart;
