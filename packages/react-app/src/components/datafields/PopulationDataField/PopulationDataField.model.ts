export type PopulationDataFieldProps = {
  readonly population?: number;
  readonly parentPopulation?: number;
  readonly parentName?: string;
};
