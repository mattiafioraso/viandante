import React, { FunctionComponent } from 'react';

import DataField from '../DataField/DataField';
import PercentagePieChart from '../PercentagePieChart/PercentagePieChart';
import Row from '../../common/Row/Row';

import { PopulationDataFieldProps } from './PopulationDataField.model';

const formatPercentage = Intl.NumberFormat(navigator.language, {
  maximumSignificantDigits: 2,
}).format;

const formatNumber = Intl.NumberFormat(navigator.language).format;

const PopulationDataField: FunctionComponent<PopulationDataFieldProps> = ({
  parentName = '',
  population = 0,
  parentPopulation = 0,
}) => {
  const populationRatio = population / parentPopulation;

  return (
    <Row>
      <PercentagePieChart value={population / parentPopulation} />
      <DataField
        label="Population"
        info={
          population > 0 &&
          parentPopulation > 0 && (
            <>
              <strong>{formatPercentage(populationRatio * 100)}%</strong> of{' '}
              {parentName && `${parentName} `}citizens live here
            </>
          )
        }
      >
        {formatNumber(population)}
      </DataField>
    </Row>
  );
};

export default PopulationDataField;
