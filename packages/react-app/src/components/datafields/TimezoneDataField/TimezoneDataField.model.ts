export type TimezoneDataFieldProps = {
  readonly offset?: number;
  readonly timezone?: string;
  readonly timezoneDST?: string;
};
