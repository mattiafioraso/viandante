import React, { FunctionComponent } from 'react';

import { TimezoneDataFieldProps } from './TimezoneDataField.model';

import Row from '../../common/Row/Row';
import Badge from '../Badge/Badge';
import DataField from '../DataField/DataField';

const TimezoneDataField: FunctionComponent<TimezoneDataFieldProps> = ({
  offset,
  timezone,
  timezoneDST,
}) => (
  <Row>
    <Badge backgroundColor="#ffe6a4" color="black">
      {typeof offset === 'number' ? (
        <>
          {offset > 0 && '+'}
          {offset}
        </>
      ) : (
        '?'
      )}
    </Badge>
    <DataField
      label="Timezone"
      info={
        timezoneDST && (
          <>
            The timezone during DST is <strong>{timezoneDST}</strong>
          </>
        )
      }
    >
      {timezone}
    </DataField>
  </Row>
);

export default TimezoneDataField;
