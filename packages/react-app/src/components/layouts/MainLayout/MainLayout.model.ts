import { ReactNode } from 'react';

export type MainLayoutProps = {
  readonly backgroundComponent?: ReactNode;
};
