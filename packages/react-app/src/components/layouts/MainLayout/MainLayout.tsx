import React, { FunctionComponent } from 'react';

import { MainLayoutProps } from './MainLayout.model';

import styled from 'styled-components';

const BackgroundComponent = styled.div`
  position: fixed;
  width: calc(100% - 640px);
  height: 100%;
  top: 0;
  right: 0;
  z-index: 0;

  @media (max-width: 960px) {
    width: 100%;
    height: 256px;
    right: auto;
  }
`;

const Main = styled.main`
  position: relative;
  box-sizing: border-box;
  width: 640px;
  min-height: 100%;
  background: ${({ theme }) => theme.palette.background.primary};
  padding: 48px;
  z-index: 1;
  box-shadow: 0 0 48px 0 rgba(0 0 0 / 0.75);

  color: ${({ theme }) => theme.palette.text.primary};

  @media (max-width: 960px) {
    width: 100%;
    top: 256px;
    min-height: calc(100% - 256px);
    padding: 32px;
  }
`;

const MainLayout: FunctionComponent<MainLayoutProps> = ({
  children,
  backgroundComponent,
}) => (
  <>
    <Main>{children}</Main>
    <BackgroundComponent>{backgroundComponent}</BackgroundComponent>
  </>
);

export default MainLayout;
