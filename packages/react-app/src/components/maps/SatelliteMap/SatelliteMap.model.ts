import { HTMLAttributes } from 'react';

export type SatelliteMapProps = Pick<
  HTMLAttributes<HTMLDivElement>,
  'className'
> & {
  readonly zoom?: number;
  readonly latitude?: number;
  readonly longitude?: number;
};
