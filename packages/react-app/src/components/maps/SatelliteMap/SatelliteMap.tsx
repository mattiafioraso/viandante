import React, { FunctionComponent } from 'react';

import styled from 'styled-components';

import { Map, TileLayer } from 'react-leaflet';

import { SatelliteMapProps } from './SatelliteMap.model';

const SatelliteMap: FunctionComponent<SatelliteMapProps> = ({
  className,
  zoom = 3,
  latitude = 0,
  longitude = 0,
}) => {
  return (
    <Map
      animate
      zoom={zoom}
      center={{ lat: latitude, lng: longitude }}
      className={className}
      dragging={false}
      touchZoom={false}
      doubleClickZoom={false}
      scrollWheelZoom={false}
      boxZoom={false}
      keyboard={false}
      zoomControl={false}
    >
      <TileLayer
        id="satellite-v9"
        accessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
        url="https://api.mapbox.com/styles/v1/mapbox/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}"
      />
    </Map>
  );
};

export default styled(SatelliteMap)`
  position: relative;
  width: 100%;
  height: 100%;
`;
