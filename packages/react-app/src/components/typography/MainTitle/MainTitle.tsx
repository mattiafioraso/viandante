import styled from 'styled-components';

export default styled.h1`
  font-family: 'Playfair Display', serif;
  line-height: 4rem;
  font-size: 4rem;
  margin: 0;
  margin-bottom: 48px;

  @media (max-width: 960px) {
    font-size: 3rem;
  }
`;
