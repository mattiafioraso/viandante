export type SectionTitleProps = {
  readonly disableMargin?: boolean;
};
