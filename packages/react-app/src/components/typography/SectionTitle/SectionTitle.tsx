import styled from 'styled-components';

import { SectionTitleProps } from './SectionTitle.model';

export default styled.h2<SectionTitleProps>`
  font-weight: 500;
  line-height: 2rem;
  font-size: 1.5rem;
  margin: 0;
  ${({ disableMargin }) => (disableMargin ? '' : `margin-bottom: 24px`)}
`;
