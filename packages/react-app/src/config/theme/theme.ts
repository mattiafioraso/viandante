import { DefaultTheme } from 'styled-components';

export const lightTheme: DefaultTheme = {
  borderRadius: '12px',
  palette: {
    text: {
      primary: 'black',
      secondary: 'rgba(0 0 0 / 0.5)',
    },
    divider: {
      primary: 'rgba(0 0 0 / 0.25)',
      secondary: 'rgba(0 0 0 / 0.25)',
    },
    main: {
      primary: '#1c454d',
      secondary: '#386a73',
      contrast: 'white',
    },
    background: {
      primary: 'white',
      secondary: '#eee',
    },
  },
};

export const darkTheme: DefaultTheme = {
  borderRadius: '12px',
  palette: {
    text: {
      primary: 'white',
      secondary: 'rgba(255 255 255 / 0.5)',
    },
    divider: {
      primary: 'rgba(255 255 255 / 0.25)',
      secondary: 'rgba(255 255 255 / 0.25)',
    },
    main: {
      primary: '#1b93ab',
      secondary: '#33a5bb',
      contrast: 'black',
    },
    background: {
      primary: '#222',
      secondary: '#333',
    },
  },
};
