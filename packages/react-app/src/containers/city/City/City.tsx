import React, { FunctionComponent, useContext, useEffect } from 'react';

import { useRouteMatch, Redirect } from 'react-router-dom';

import { useCityQuery } from '../../../generated/graphql';

import { GeolocationContext } from '../../../providers/GeolocationProvider/GeolocationProvider';

import PopulationDataField from '../../../components/datafields/PopulationDataField/PopulationDataField';
import TimezoneDataField from '../../../components/datafields/TimezoneDataField/TimezoneDataField';
import Loader from '../../../components/common/Loader/Loader';

const City: FunctionComponent = () => {
  const {
    url,
    params: { cityId },
  } = useRouteMatch<{ readonly cityId: string }>();

  const { data: { cities: [city] = [] } = {}, loading } = useCityQuery({
    variables: { id: cityId },
  });

  const [, setGeolocation] = useContext(GeolocationContext);

  useEffect(() => {
    if (city) {
      const { lat, long } = city.location ?? {};

      setGeolocation({
        zoom: 11,
        latitude: lat ?? 0,
        longitude: long ?? 0,
      });
    }
  }, [setGeolocation, city]);

  if (loading) {
    return <Loader />;
  }

  if (!city) {
    return <Redirect to={url.slice(0, url.lastIndexOf('/'))} />;
  }

  return (
    <section>
      <PopulationDataField
        population={city.population}
        parentName={city.country.name}
        parentPopulation={city.country.population}
      />

      {city.timeZone && (
        <TimezoneDataField
          offset={city.timeZone.offset}
          timezone={city.timeZone.name}
          timezoneDST={city.timeZoneDST?.name}
        />
      )}
    </section>
  );
};

export default City;
