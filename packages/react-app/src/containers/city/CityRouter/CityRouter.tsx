import React, { FunctionComponent } from 'react';

import { Route, Redirect, Switch, useRouteMatch } from 'react-router-dom';

import loadable from '@loadable/component';

const City = loadable(() => import('../City/City'));

const CityRouter: FunctionComponent = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route exact path={path}>
        <City />
      </Route>
      <Redirect to={path} />
    </Switch>
  );
};

export default CityRouter;
