import React, { FunctionComponent } from 'react';

import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

import loadable from '@loadable/component';

import { ThemeProvider } from 'styled-components';

import { lightTheme } from '../../../config/theme/theme';

import GraphQLProvider from '../../../providers/GraphQLProvider/GraphQLProvider';
import GeolocationProvider from '../../../providers/GeolocationProvider/GeolocationProvider';

import MainContainer from '../MainContainer/MainContainer';

const Home = loadable(() => import('../Home/Home'));

const ContinentRouter = loadable(
  () => import('../../continent/ContinentRouter/ContinentRouter'),
);

const App: FunctionComponent = () => (
  <GraphQLProvider>
    <ThemeProvider theme={lightTheme}>
      <BrowserRouter>
        <GeolocationProvider>
          <Route path="/:continentId?/:countryId?/:cityId?">
            <MainContainer>
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/:continentId">
                  <ContinentRouter />
                </Route>
                <Redirect to="/" />
              </Switch>
            </MainContainer>
          </Route>
        </GeolocationProvider>
      </BrowserRouter>
    </ThemeProvider>
  </GraphQLProvider>
);

export default App;
