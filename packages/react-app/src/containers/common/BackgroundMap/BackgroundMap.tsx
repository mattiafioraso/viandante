import React, { FunctionComponent, useContext } from 'react';

import SatelliteMap from '../../../components/maps/SatelliteMap/SatelliteMap';

import { GeolocationContext } from '../../../providers/GeolocationProvider/GeolocationProvider';

const BackgroundMap: FunctionComponent = () => {
  const [location] = useContext(GeolocationContext);

  return <SatelliteMap {...location} />;
};

export default BackgroundMap;
