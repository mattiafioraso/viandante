import React, { FunctionComponent, useContext, useEffect } from 'react';

import { useContinentsQuery } from '../../../generated/graphql';

import { GeolocationContext } from '../../../providers/GeolocationProvider/GeolocationProvider';

import Row from '../../../components/common/Row/Row';
import SectionTitle from '../../../components/typography/SectionTitle/SectionTitle';
import PlaceCard from '../../../components/common/PlaceCard/PlaceCard';
import Loader from '../../../components/common/Loader/Loader';

const Home: FunctionComponent = () => {
  const { data, loading } = useContinentsQuery();

  const [, setGeolocation] = useContext(GeolocationContext);

  useEffect(() => {
    setGeolocation({ latitude: 0, longitude: 0, zoom: 2 });
  }, [setGeolocation]);

  return (
    <>
      <section>
        <SectionTitle>About</SectionTitle>
        <p>
          <strong>Viandante</strong> (italian word for "wanderer") is a traveler
          platform that allows users to discover new places.
        </p>
        <p>It is built using React and relies upon GraphQL to retrieve data.</p>
      </section>
      <section>
        <SectionTitle>Where do you want to go?</SectionTitle>
        {loading ? (
          <Loader />
        ) : (
          data?.continents.map(({ id, name }) => (
            <Row disableMargin key={id}>
              <PlaceCard title={name} to={id} />
            </Row>
          ))
        )}
      </section>
    </>
  );
};

export default Home;
