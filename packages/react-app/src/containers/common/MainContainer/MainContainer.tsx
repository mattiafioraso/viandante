import React, { FunctionComponent, useEffect, Suspense } from 'react';

import loadable from '@loadable/component';

import MainLayout from '../../../components/layouts/MainLayout/MainLayout';
import MainTitle from '../../../components/typography/MainTitle/MainTitle';
import Breadcrumbs from '../../../components/common/Breadcrumbs/Breadcrumbs';
import Loader from '../../../components/common/Loader/Loader';

import { Link, useRouteMatch } from 'react-router-dom';
import {
  useContinentLazyQuery,
  useCountryLazyQuery,
  useCityLazyQuery,
} from '../../../generated/graphql';

const BackgroundMap = loadable(() => import('../BackgroundMap/BackgroundMap'));

const MainContainer: FunctionComponent = ({ children }) => {
  const {
    params: { continentId, countryId, cityId },
  } = useRouteMatch<{
    readonly continentId?: string;
    readonly countryId?: string;
    readonly cityId?: string;
  }>();

  // Continent ----------------------------------------------------------------
  const [
    getContinent,
    { data: continentData, loading: loadingContinent },
  ] = useContinentLazyQuery();

  useEffect(() => {
    if (continentId) {
      getContinent({ variables: { id: continentId } });
    }
  }, [getContinent, continentId]);

  const [{ name: continentName = null } = {}] = continentData?.continents ?? [];

  // Country ------------------------------------------------------------------
  const [
    getCountry,
    { data: countryData, loading: loadingCountry },
  ] = useCountryLazyQuery();

  useEffect(() => {
    if (countryId) {
      getCountry({ variables: { id: countryId } });
    }
  }, [getCountry, countryId]);

  const [{ name: countryName = null } = {}] = countryData?.countries ?? [];

  // City ---------------------------------------------------------------------
  const [
    getCity,
    { data: cityData, loading: loadingCity },
  ] = useCityLazyQuery();

  useEffect(() => {
    if (cityId) {
      getCity({ variables: { id: cityId } });
    }
  }, [getCity, cityId]);

  const [{ name: cityName = null } = {}] = cityData?.cities ?? [];

  // --------------------------------------------------------------------------

  const title =
    (cityId && cityName) ||
    (countryId && countryName) ||
    (continentId && continentName) ||
    'Viandante';

  return (
    <MainLayout
      backgroundComponent={
        <Suspense fallback={<Loader />}>
          <BackgroundMap />
        </Suspense>
      }
    >
      {loadingContinent || loadingCountry || loadingCity ? (
        <Loader />
      ) : (
        <>
          <MainTitle>{title}</MainTitle>

          {continentId && (
            <Breadcrumbs>
              <>
                <Link to="/">Home</Link>
                {!loadingContinent && (
                  <>
                    <Link to={`/${continentId}`}>{continentName}</Link>
                    {countryId && !loadingCountry && (
                      <>
                        <Link to={`/${continentId}/${countryId}`}>
                          {countryName}
                        </Link>
                        {cityId && !loadingCity && (
                          <>
                            <Link to={`/${continentId}/${countryId}/${cityId}`}>
                              {cityName}
                            </Link>
                          </>
                        )}
                      </>
                    )}
                  </>
                )}
              </>
            </Breadcrumbs>
          )}
          <Suspense fallback={<Loader />}>{children}</Suspense>
        </>
      )}
    </MainLayout>
  );
};

export default MainContainer;
