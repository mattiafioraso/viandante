import React, { FunctionComponent, useContext, useEffect } from 'react';

import { useRouteMatch, Redirect } from 'react-router-dom';

import { useContinentQuery } from '../../../generated/graphql';

import { GeolocationContext } from '../../../providers/GeolocationProvider/GeolocationProvider';

import Row from '../../../components/common/Row/Row';
import Loader from '../../../components/common/Loader/Loader';
import PlaceCard from '../../../components/common/PlaceCard/PlaceCard';
import SearchableList from '../../../components/common/SearchableList/SearchableList';
import SectionTitle from '../../../components/typography/SectionTitle/SectionTitle';
import PopulationDataField from '../../../components/datafields/PopulationDataField/PopulationDataField';

const Continent: FunctionComponent = () => {
  const {
    url,
    params: { continentId },
  } = useRouteMatch<{ readonly continentId: string }>();

  const [, setGeolocation] = useContext(GeolocationContext);

  const { data, loading } = useContinentQuery({
    variables: { id: continentId },
  });

  const [continent] = data?.continents ?? [];

  const worldPopulation = data?.worldContinents?.reduce(
    (total, { population }) => total + population,
    0,
  );

  useEffect(() => {
    if (Array.isArray(continent?.countries)) {
      const [country] = continent.countries;

      if (country) {
        const { location } = country;

        setGeolocation({
          latitude: location?.lat ?? 0,
          longitude: location?.long ?? 0,
          zoom: 3,
        });
      }
    }
  }, [continent, setGeolocation]);

  if (loading) {
    return <Loader />;
  }

  if (!continent) {
    return <Redirect to={url.slice(0, url.lastIndexOf('/'))} />;
  }

  const countries = continent?.countries ?? [];

  return (
    <>
      <section>
        <SectionTitle>Information</SectionTitle>
        <PopulationDataField
          population={continent.population}
          parentName="Earth"
          parentPopulation={worldPopulation}
        />
      </section>
      {countries.length > 0 && (
        <section>
          <SectionTitle disableMargin>Countries</SectionTitle>
          <SearchableList
            items={continent?.countries ?? []}
            searchKey="name"
            idKey="id"
            renderItem={({ id, name }) => (
              <Row disableMargin>
                <PlaceCard title={name} to={`${url}/${id}`} />
              </Row>
            )}
          />
        </section>
      )}
    </>
  );
};

export default Continent;
