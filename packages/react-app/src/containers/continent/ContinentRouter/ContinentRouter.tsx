import React, { FunctionComponent } from 'react';

import { Route, Redirect, Switch, useRouteMatch } from 'react-router-dom';

import loadable from '@loadable/component';

const Continent = loadable(() => import('../Continent/Continent'));

const CountryRouter = loadable(
  () => import('../../country/CountryRouter/CountryRouter'),
);

const ContinentRouter: FunctionComponent = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route exact path={path}>
        <Continent />
      </Route>
      <Route path={`${path}/:countryId`}>
        <CountryRouter />
      </Route>
      <Redirect to={path} />
    </Switch>
  );
};

export default ContinentRouter;
