import React, { FunctionComponent, useContext, useEffect } from 'react';

import { useRouteMatch, Redirect } from 'react-router-dom';

import { useCountryQuery } from '../../../generated/graphql';

import { GeolocationContext } from '../../../providers/GeolocationProvider/GeolocationProvider';

import PlaceCard from '../../../components/common/PlaceCard/PlaceCard';
import SearchableList from '../../../components/common/SearchableList/SearchableList';
import Row from '../../../components/common/Row/Row';
import SectionTitle from '../../../components/typography/SectionTitle/SectionTitle';
import PopulationDataField from '../../../components/datafields/PopulationDataField/PopulationDataField';
import LanguageDataField from '../../../components/datafields/LanguageDataField/LanguageDataField';
import CurrencyDataField from '../../../components/datafields/CurrencyDataField/CurrencyDataField';
import Loader from '../../../components/common/Loader/Loader';

const Country: FunctionComponent = () => {
  const {
    url,
    params: { countryId },
  } = useRouteMatch<{ readonly countryId: string }>();

  const [, setGeolocation] = useContext(GeolocationContext);

  const { data, loading } = useCountryQuery({
    variables: { id: countryId },
  });

  const [country] = data?.countries ?? [];

  useEffect(() => {
    if (country) {
      setGeolocation({
        latitude: country.location?.lat ?? 0,
        longitude: country.location?.long ?? 0,
        zoom: 5,
      });
    }
  }, [country, setGeolocation]);

  if (loading) {
    return <Loader />;
  }

  if (!country) {
    return <Redirect to={url.slice(0, url.lastIndexOf('/'))} />;
  }

  const capital = country?.capital;
  const cities = country?.cities ?? [];
  const [mainLanguage, ...otherLanguages] = country?.languages ?? [];
  const [mainCurrency, ...otherCurrencies] = country?.currencies ?? [];

  return (
    <>
      <section>
        <SectionTitle>Information</SectionTitle>
        <PopulationDataField
          population={country.population}
          parentName={country.continent.name}
          parentPopulation={country.continent.population}
        />
        {mainCurrency && (
          <CurrencyDataField
            unitSymbol={mainCurrency.unitSymbols?.[0]}
            main={
              mainCurrency.name?.[0]?.toLocaleUpperCase() +
              mainCurrency.name.slice(1)
            }
            other={otherCurrencies.map(({ name }) => name)}
          />
        )}
        {mainLanguage && (
          <LanguageDataField
            main={mainLanguage.name}
            other={otherLanguages.map(({ name }) => name)}
          />
        )}
      </section>

      {capital && (
        <section>
          <SectionTitle>Capital</SectionTitle>
          <Row>
            <PlaceCard title={capital.name} to={`${url}/${capital.id}`} />
          </Row>
        </section>
      )}

      {cities.length > 0 && (
        <section>
          <SectionTitle disableMargin>
            {capital ? 'Other cities' : 'Cities'}
          </SectionTitle>
          <SearchableList
            items={cities}
            searchKey="name"
            idKey="id"
            renderItem={({ id, name }) => (
              <Row disableMargin>
                <PlaceCard title={name} to={`${url}/${id}`} />
              </Row>
            )}
          />
        </section>
      )}
    </>
  );
};

export default Country;
