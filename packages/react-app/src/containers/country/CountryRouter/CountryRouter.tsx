import React, { FunctionComponent } from 'react';

import { Route, Redirect, Switch, useRouteMatch } from 'react-router-dom';

import loadable from '@loadable/component';

const Country = loadable(() => import('../Country/Country'));

const CityRouter = loadable(() => import('../../city/CityRouter/CityRouter'));

const CountryRouter: FunctionComponent = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route exact path={path}>
        <Country />
      </Route>
      <Route path={`${path}/:cityId`}>
        <CityRouter />
      </Route>
      <Redirect to={path} />
    </Switch>
  );
};

export default CountryRouter;
