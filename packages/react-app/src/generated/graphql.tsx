import { gql } from 'graphql.macro';
import * as ApolloReactCommon from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { readonly [key: string]: unknown }> = {
  readonly [K in keyof T]: T[K];
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  readonly ID: string;
  readonly String: string;
  readonly Boolean: boolean;
  readonly Int: number;
  readonly Float: number;
};

/** A city is a large human settlement. */
export type City = {
  readonly __typename?: 'City';
  /** The continent. */
  readonly continent: Continent;
  /** The country. */
  readonly country: Country;
  /** The Geonames.org ID. */
  readonly geonamesID: Scalars['Int'];
  /** The Wikidata ID. */
  readonly id: Scalars['String'];
  /** The location. */
  readonly location?: Maybe<Coordinates>;
  /** The name. */
  readonly name: Scalars['String'];
  /** The population. */
  readonly population: Scalars['Int'];
  /** The regular time zone (non-DST). */
  readonly timeZone?: Maybe<TimeZone>;
  /** The time zone during daylight savings time. */
  readonly timeZoneDST?: Maybe<TimeZone>;
};

export type CityWhere = {
  readonly id?: Maybe<WhereString>;
  readonly name?: Maybe<WhereString>;
  readonly countryName?: Maybe<WhereString>;
  readonly population?: Maybe<WhereFloat>;
};

/** Information about the client that sent the request. */
export type Client = {
  readonly __typename?: 'Client';
  /** The IP address. */
  readonly ipAddress: IpAddress;
  /** The user agent. */
  readonly userAgent: Scalars['String'];
};

/** A continent is one of several very large landmasses. */
export type Continent = {
  readonly __typename?: 'Continent';
  /** All countries located on the continent. */
  readonly countries: ReadonlyArray<Country>;
  /** The Geonames.org ID. */
  readonly geonamesID: Scalars['Int'];
  /** The Wikidata ID. */
  readonly id: Scalars['String'];
  /** The name. */
  readonly name: Scalars['String'];
  /** The population. */
  readonly population: Scalars['Int'];
};

export type ContinentWhere = {
  readonly id?: Maybe<WhereString>;
  readonly name?: Maybe<WhereString>;
  readonly geonamesId?: Maybe<WhereInt>;
};

/** Geographic coordinates. */
export type Coordinates = {
  readonly __typename?: 'Coordinates';
  /** Latitude. */
  readonly lat: Scalars['Float'];
  /** Longitude */
  readonly long: Scalars['Float'];
};

/** A sovereign state. */
export type Country = {
  readonly __typename?: 'Country';
  /** The ISO 3166-1 alpha-2 code. */
  readonly alpha2Code: Scalars['String'];
  /** The ISO 3166-1 alpha-3 code. */
  readonly alpha3Code: Scalars['String'];
  /** Calling codes. */
  readonly callingCodes: ReadonlyArray<Scalars['String']>;
  /** The capital city. */
  readonly capital?: Maybe<City>;
  /** All cities of the country. */
  readonly cities: ReadonlyArray<City>;
  /** The continent the country is located in. */
  readonly continent: Continent;
  /** All official currencies of the country. */
  readonly currencies: ReadonlyArray<Currency>;
  /** The Geonames.org ID. */
  readonly geonamesID: Scalars['Int'];
  /** The Wikidata ID. */
  readonly id: Scalars['String'];
  /** All official languages of the country. */
  readonly languages: ReadonlyArray<Language>;
  /** The location. */
  readonly location?: Maybe<Coordinates>;
  /** The name. */
  readonly name: Scalars['String'];
  /** The population. */
  readonly population: Scalars['Int'];
  /** The general VAT rate. */
  readonly vatRate?: Maybe<Scalars['Float']>;
};

export type CountryWhere = {
  readonly id?: Maybe<WhereString>;
  readonly name?: Maybe<WhereString>;
  readonly alpha2Code?: Maybe<WhereString>;
  readonly alpha3Code?: Maybe<WhereString>;
  readonly population?: Maybe<WhereInt>;
};

export type Currency = {
  readonly __typename?: 'Currency';
  /** Convert `amount` (default 1) to currency with code specified in `to` */
  readonly convert?: Maybe<Scalars['Float']>;
  /** Countries that use the currency. */
  readonly countries: ReadonlyArray<Country>;
  /** The Wikidata ID. */
  readonly id: Scalars['String'];
  /** The ISO 4217 code. */
  readonly isoCode: Scalars['String'];
  /** The name. */
  readonly name: Scalars['String'];
  /** Unit symbols. */
  readonly unitSymbols: ReadonlyArray<Scalars['String']>;
};

export type CurrencyConvertArgs = {
  readonly amount?: Maybe<Scalars['Float']>;
  readonly to: Scalars['String'];
};

export type CurrencyWhere = {
  readonly id?: Maybe<WhereString>;
  readonly name?: Maybe<WhereString>;
  readonly isoCode?: Maybe<WhereString>;
};

export type DnsRecords = {
  readonly __typename?: 'DNSRecords';
  readonly a: ReadonlyArray<IpAddress>;
  readonly aaaa: ReadonlyArray<IpAddress>;
  readonly cname: ReadonlyArray<DomainName>;
  readonly mx: ReadonlyArray<MxRecord>;
};

/** Domain Name of the Domain Name System (DNS). */
export type DomainName = {
  readonly __typename?: 'DomainName';
  /** Look up A records. */
  readonly a: ReadonlyArray<IpAddress>;
  /** Look up AAAA records. */
  readonly aaaa: ReadonlyArray<IpAddress>;
  /** Look up CNAME records. */
  readonly cname: ReadonlyArray<DomainName>;
  /** Look up MX records. */
  readonly mx: ReadonlyArray<MxRecord>;
  /** The domain name. */
  readonly name: Scalars['String'];
  /** @deprecated Use fields on domainName itself */
  readonly records: DnsRecords;
};

export type EmailAddress = {
  readonly __typename?: 'EmailAddress';
  /** The email address. */
  readonly address: Scalars['String'];
  /** The host as a domain name. */
  readonly domainName: DomainName;
  /** Host part of the email address (after the @) */
  readonly host: Scalars['String'];
  /** Local part of the email address (before the @) */
  readonly local: Scalars['String'];
  /** Whether this address is backed by a working mail server */
  readonly ok: Scalars['Boolean'];
  readonly serviceProvider?: Maybe<EmailServiceProvider>;
};

export type EmailServiceProvider = {
  readonly __typename?: 'EmailServiceProvider';
  /** Whether this provider offers mailboxes without the need for signup */
  readonly disposable: Scalars['Boolean'];
  /** The domain name. */
  readonly domainName: DomainName;
  /** Whether this provider offers mailboxes at no cost */
  readonly free: Scalars['Boolean'];
  /** Whether this provider's SMTP service is working */
  readonly smtpOk: Scalars['Boolean'];
};

export type HtmlDocument = {
  readonly __typename?: 'HTMLDocument';
  /** Get all nodes that match selector. */
  readonly all: ReadonlyArray<HtmlNode>;
  /** Document body. */
  readonly body: HtmlNode;
  /** Get first node that matches selector. */
  readonly first?: Maybe<HtmlNode>;
  /** Raw HTML representation. */
  readonly html: Scalars['String'];
  /** Document title. */
  readonly title?: Maybe<Scalars['String']>;
};

export type HtmlDocumentAllArgs = {
  readonly selector: Scalars['String'];
};

export type HtmlDocumentFirstArgs = {
  readonly selector: Scalars['String'];
};

export type HtmlNode = {
  readonly __typename?: 'HTMLNode';
  /** Get all nodes that match selector. */
  readonly all: ReadonlyArray<HtmlNode>;
  /** Attribute value. */
  readonly attribute?: Maybe<Scalars['String']>;
  /** Child nodes. */
  readonly children: ReadonlyArray<HtmlNode>;
  /** Get first node that matches selector. */
  readonly first?: Maybe<HtmlNode>;
  /** Raw HTML representation. */
  readonly html: Scalars['String'];
  /** Next node. */
  readonly next?: Maybe<HtmlNode>;
  /** Parent node. */
  readonly parent?: Maybe<HtmlNode>;
  /** Previous node. */
  readonly previous?: Maybe<HtmlNode>;
  /** Inner text. */
  readonly text?: Maybe<Scalars['String']>;
};

export type HtmlNodeAllArgs = {
  readonly selector: Scalars['String'];
};

export type HtmlNodeAttributeArgs = {
  readonly name: Scalars['String'];
  readonly selector?: Maybe<Scalars['String']>;
};

export type HtmlNodeFirstArgs = {
  readonly selector: Scalars['String'];
};

export type HtmlNodeTextArgs = {
  readonly selector?: Maybe<Scalars['String']>;
};

/**
 * Internet Protocol address. Can be either a IPv4 or a IPv6 address.
 *
 * This product includes GeoLite2 data created by MaxMind, available from www.maxmind.com.
 */
export type IpAddress = {
  readonly __typename?: 'IPAddress';
  /** The IP address. */
  readonly address: Scalars['String'];
  /** The city this IP address belongs to. */
  readonly city?: Maybe<City>;
  /** The country this IP address belongs to. */
  readonly country?: Maybe<Country>;
  /** The IP address type. */
  readonly type: IpAddressType;
};

export enum IpAddressType {
  IPV4 = 'IPv4',
  IPV6 = 'IPv6',
}

export type Language = {
  readonly __typename?: 'Language';
  /** The ISO 639-1 code. */
  readonly alpha2Code: Scalars['String'];
  /** The countries that use the language. */
  readonly countries: ReadonlyArray<Country>;
  /** The Wikidata ID. */
  readonly id: Scalars['String'];
  /** The name. */
  readonly name: Scalars['String'];
};

export type LanguageWhere = {
  readonly id?: Maybe<WhereString>;
  readonly name?: Maybe<WhereString>;
  readonly alpha2Code?: Maybe<WhereString>;
};

export type MxRecord = {
  readonly __typename?: 'MXRecord';
  /** The domain name. */
  readonly exchange: DomainName;
  /** The preference value. */
  readonly preference: Scalars['Int'];
};

export type Markdown = {
  readonly __typename?: 'Markdown';
  /** Convert markdown to HTML */
  readonly html: Scalars['String'];
};

/** Query is the root object of all queries. */
export type Query = {
  readonly __typename?: 'Query';
  /** Get cities. */
  readonly cities: ReadonlyArray<City>;
  /** Get client info. */
  readonly client: Client;
  /** Get continents. */
  readonly continents: ReadonlyArray<Continent>;
  /** Get countries. */
  readonly countries: ReadonlyArray<Country>;
  /** Get currencies. */
  readonly currencies: ReadonlyArray<Currency>;
  /** Create `DomainName` from string. */
  readonly domainName: DomainName;
  /** Create `EmailAddress` from string. */
  readonly emailAddress: EmailAddress;
  /** Create `HTMLDocument` from string. */
  readonly htmlDocument: HtmlDocument;
  /** Create `IPAddress` from string. */
  readonly ipAddress: IpAddress;
  /** Get languages. */
  readonly languages: ReadonlyArray<Language>;
  /** Create `Markdown` from string. */
  readonly markdown: Markdown;
  readonly random: Random;
  /** Get time zones. */
  readonly timeZones: ReadonlyArray<TimeZone>;
  /** Create `URL` from string. */
  readonly url: Url;
};

/** Query is the root object of all queries. */
export type QueryCitiesArgs = {
  readonly limit?: Maybe<Scalars['Int']>;
  readonly skip?: Maybe<Scalars['Int']>;
  readonly where?: Maybe<CityWhere>;
};

/** Query is the root object of all queries. */
export type QueryContinentsArgs = {
  readonly limit?: Maybe<Scalars['Int']>;
  readonly skip?: Maybe<Scalars['Int']>;
  readonly where?: Maybe<ContinentWhere>;
};

/** Query is the root object of all queries. */
export type QueryCountriesArgs = {
  readonly limit?: Maybe<Scalars['Int']>;
  readonly skip?: Maybe<Scalars['Int']>;
  readonly where?: Maybe<CountryWhere>;
};

/** Query is the root object of all queries. */
export type QueryCurrenciesArgs = {
  readonly limit?: Maybe<Scalars['Int']>;
  readonly skip?: Maybe<Scalars['Int']>;
  readonly where?: Maybe<CurrencyWhere>;
};

/** Query is the root object of all queries. */
export type QueryDomainNameArgs = {
  readonly name: Scalars['String'];
};

/** Query is the root object of all queries. */
export type QueryEmailAddressArgs = {
  readonly address: Scalars['String'];
};

/** Query is the root object of all queries. */
export type QueryHtmlDocumentArgs = {
  readonly html: Scalars['String'];
};

/** Query is the root object of all queries. */
export type QueryIpAddressArgs = {
  readonly address: Scalars['String'];
};

/** Query is the root object of all queries. */
export type QueryLanguagesArgs = {
  readonly limit?: Maybe<Scalars['Int']>;
  readonly skip?: Maybe<Scalars['Int']>;
  readonly where?: Maybe<LanguageWhere>;
};

/** Query is the root object of all queries. */
export type QueryMarkdownArgs = {
  readonly text: Scalars['String'];
};

/** Query is the root object of all queries. */
export type QueryRandomArgs = {
  readonly cacheBuster?: Maybe<Scalars['String']>;
};

/** Query is the root object of all queries. */
export type QueryTimeZonesArgs = {
  readonly limit?: Maybe<Scalars['Int']>;
  readonly skip?: Maybe<Scalars['Int']>;
  readonly where?: Maybe<TimeZoneWhere>;
};

/** Query is the root object of all queries. */
export type QueryUrlArgs = {
  readonly url: Scalars['String'];
};

/** Cryptographically secure random number generator. */
export type Random = {
  readonly __typename?: 'Random';
  /** Generate a float. */
  readonly float: Scalars['Int'];
  /** Generate a integer. */
  readonly int: Scalars['Int'];
  /** Generate a string. */
  readonly string: Scalars['String'];
};

/** Cryptographically secure random number generator. */
export type RandomFloatArgs = {
  readonly high?: Maybe<Scalars['Float']>;
  readonly low?: Maybe<Scalars['Float']>;
};

/** Cryptographically secure random number generator. */
export type RandomIntArgs = {
  readonly high?: Maybe<Scalars['Int']>;
  readonly low?: Maybe<Scalars['Int']>;
};

/** Cryptographically secure random number generator. */
export type RandomStringArgs = {
  readonly length?: Maybe<Scalars['Int']>;
};

/** Time zone offset from UTC. */
export type TimeZone = {
  readonly __typename?: 'TimeZone';
  /** Cities in this time zone. */
  readonly cities: ReadonlyArray<City>;
  /** The Wikidata ID. */
  readonly id: Scalars['String'];
  /** The name. */
  readonly name: Scalars['String'];
  /** The UTC offset. */
  readonly offset: Scalars['Float'];
};

export type TimeZoneWhere = {
  readonly id?: Maybe<WhereString>;
  readonly name?: Maybe<WhereString>;
  readonly offset?: Maybe<WhereFloat>;
};

/** Uniform Resource Locator (URL) in the form `<scheme>://<host><:port>/<path>?<query>`. */
export type Url = {
  readonly __typename?: 'URL';
  /** The host as a domain name. */
  readonly domainName?: Maybe<DomainName>;
  /** The host. */
  readonly host: Scalars['String'];
  /** Fetches the URL and returns `HTMLDocument`. Does not resolve redirects. Returns an error if the request fails or null if the response is not a HTML document. */
  readonly htmlDocument?: Maybe<HtmlDocument>;
  /** The path. */
  readonly path?: Maybe<Scalars['String']>;
  /** The port. */
  readonly port?: Maybe<Scalars['Int']>;
  /** The query. */
  readonly query?: Maybe<Scalars['String']>;
  /** The scheme. */
  readonly scheme: Scalars['String'];
  /** The full URL. */
  readonly url: Scalars['String'];
};

export type WhereFloat = {
  readonly eq?: Maybe<Scalars['Float']>;
  readonly neq?: Maybe<Scalars['Float']>;
  readonly in?: Maybe<ReadonlyArray<Scalars['Float']>>;
  readonly nin?: Maybe<ReadonlyArray<Scalars['Float']>>;
  readonly lt?: Maybe<Scalars['Float']>;
  readonly gt?: Maybe<Scalars['Float']>;
};

export type WhereInt = {
  readonly eq?: Maybe<Scalars['Int']>;
  readonly neq?: Maybe<Scalars['Int']>;
  readonly in?: Maybe<ReadonlyArray<Scalars['Int']>>;
  readonly nin?: Maybe<ReadonlyArray<Scalars['Int']>>;
  readonly lt?: Maybe<Scalars['Int']>;
  readonly gt?: Maybe<Scalars['Int']>;
};

export type WhereString = {
  readonly eq?: Maybe<Scalars['String']>;
  readonly neq?: Maybe<Scalars['String']>;
  readonly in?: Maybe<ReadonlyArray<Scalars['String']>>;
  readonly nin?: Maybe<ReadonlyArray<Scalars['String']>>;
};

export type CityQueryVariables = Exact<{
  readonly id: Scalars['String'];
}>;

export type CityQuery = { readonly __typename?: 'Query' } & {
  readonly cities: ReadonlyArray<
    { readonly __typename?: 'City' } & Pick<City, 'name' | 'population'> & {
        readonly timeZone?: Maybe<
          { readonly __typename?: 'TimeZone' } & Pick<
            TimeZone,
            'name' | 'offset'
          >
        >;
        readonly timeZoneDST?: Maybe<
          { readonly __typename?: 'TimeZone' } & Pick<
            TimeZone,
            'name' | 'offset'
          >
        >;
        readonly location?: Maybe<
          { readonly __typename?: 'Coordinates' } & Pick<
            Coordinates,
            'lat' | 'long'
          >
        >;
        readonly country: { readonly __typename?: 'Country' } & Pick<
          Country,
          'id' | 'name' | 'population'
        > & {
            readonly continent: { readonly __typename?: 'Continent' } & Pick<
              Continent,
              'id' | 'name'
            >;
          };
      }
  >;
};

export type ContinentsQueryVariables = Exact<{ readonly [key: string]: never }>;

export type ContinentsQuery = { readonly __typename?: 'Query' } & {
  readonly continents: ReadonlyArray<
    { readonly __typename?: 'Continent' } & Pick<Continent, 'id' | 'name'>
  >;
};

export type ContinentTitleQueryVariables = Exact<{
  readonly id: Scalars['String'];
}>;

export type ContinentTitleQuery = { readonly __typename?: 'Query' } & {
  readonly continents: ReadonlyArray<
    { readonly __typename?: 'Continent' } & Pick<Continent, 'name'>
  >;
};

export type CountryTitleQueryVariables = Exact<{
  readonly id: Scalars['String'];
}>;

export type CountryTitleQuery = { readonly __typename?: 'Query' } & {
  readonly countries: ReadonlyArray<
    { readonly __typename?: 'Country' } & Pick<Country, 'name'>
  >;
};

export type CityTitleQueryVariables = Exact<{
  readonly id: Scalars['String'];
}>;

export type CityTitleQuery = { readonly __typename?: 'Query' } & {
  readonly cities: ReadonlyArray<
    { readonly __typename?: 'City' } & Pick<City, 'name'>
  >;
};

export type ContinentQueryVariables = Exact<{
  readonly id: Scalars['String'];
}>;

export type ContinentQuery = { readonly __typename?: 'Query' } & {
  readonly continents: ReadonlyArray<
    { readonly __typename?: 'Continent' } & Pick<
      Continent,
      'id' | 'name' | 'population'
    > & {
        readonly countries: ReadonlyArray<
          { readonly __typename?: 'Country' } & Pick<Country, 'id' | 'name'> & {
              readonly location?: Maybe<
                { readonly __typename?: 'Coordinates' } & Pick<
                  Coordinates,
                  'lat' | 'long'
                >
              >;
            }
        >;
      }
  >;
  readonly worldContinents: ReadonlyArray<
    { readonly __typename?: 'Continent' } & Pick<Continent, 'population'>
  >;
};

export type CountryQueryVariables = Exact<{
  readonly id: Scalars['String'];
}>;

export type CountryQuery = { readonly __typename?: 'Query' } & {
  readonly countries: ReadonlyArray<
    { readonly __typename?: 'Country' } & Pick<
      Country,
      'id' | 'name' | 'population'
    > & {
        readonly currencies: ReadonlyArray<
          { readonly __typename?: 'Currency' } & Pick<
            Currency,
            'name' | 'unitSymbols'
          >
        >;
        readonly languages: ReadonlyArray<
          { readonly __typename?: 'Language' } & Pick<Language, 'name'>
        >;
        readonly location?: Maybe<
          { readonly __typename?: 'Coordinates' } & Pick<
            Coordinates,
            'lat' | 'long'
          >
        >;
        readonly capital?: Maybe<
          { readonly __typename?: 'City' } & Pick<City, 'id' | 'name'>
        >;
        readonly cities: ReadonlyArray<
          { readonly __typename?: 'City' } & Pick<City, 'id' | 'name'>
        >;
        readonly continent: { readonly __typename?: 'Continent' } & Pick<
          Continent,
          'name' | 'population'
        >;
      }
  >;
};

export const CityDocument = gql`
  query City($id: String!) {
    cities(where: { id: { eq: $id } }) {
      name
      population
      timeZone {
        name
        offset
      }
      timeZoneDST {
        name
        offset
      }
      location {
        lat
        long
      }
      country {
        id
        name
        population
        continent {
          id
          name
        }
      }
    }
  }
`;

/**
 * __useCityQuery__
 *
 * To run a query within a React component, call `useCityQuery` and pass it any options that fit your needs.
 * When your component renders, `useCityQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCityQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCityQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CityQuery,
    CityQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<CityQuery, CityQueryVariables>(
    CityDocument,
    baseOptions,
  );
}
export function useCityLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CityQuery,
    CityQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<CityQuery, CityQueryVariables>(
    CityDocument,
    baseOptions,
  );
}
export type CityQueryHookResult = ReturnType<typeof useCityQuery>;
export type CityLazyQueryHookResult = ReturnType<typeof useCityLazyQuery>;
export type CityQueryResult = ApolloReactCommon.QueryResult<
  CityQuery,
  CityQueryVariables
>;
export const ContinentsDocument = gql`
  query Continents {
    continents {
      id
      name
    }
  }
`;

/**
 * __useContinentsQuery__
 *
 * To run a query within a React component, call `useContinentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useContinentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useContinentsQuery({
 *   variables: {
 *   },
 * });
 */
export function useContinentsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    ContinentsQuery,
    ContinentsQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<ContinentsQuery, ContinentsQueryVariables>(
    ContinentsDocument,
    baseOptions,
  );
}
export function useContinentsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    ContinentsQuery,
    ContinentsQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<
    ContinentsQuery,
    ContinentsQueryVariables
  >(ContinentsDocument, baseOptions);
}
export type ContinentsQueryHookResult = ReturnType<typeof useContinentsQuery>;
export type ContinentsLazyQueryHookResult = ReturnType<
  typeof useContinentsLazyQuery
>;
export type ContinentsQueryResult = ApolloReactCommon.QueryResult<
  ContinentsQuery,
  ContinentsQueryVariables
>;
export const ContinentTitleDocument = gql`
  query ContinentTitle($id: String!) {
    continents(where: { id: { eq: $id } }, limit: 1) {
      name
    }
  }
`;

/**
 * __useContinentTitleQuery__
 *
 * To run a query within a React component, call `useContinentTitleQuery` and pass it any options that fit your needs.
 * When your component renders, `useContinentTitleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useContinentTitleQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useContinentTitleQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    ContinentTitleQuery,
    ContinentTitleQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<
    ContinentTitleQuery,
    ContinentTitleQueryVariables
  >(ContinentTitleDocument, baseOptions);
}
export function useContinentTitleLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    ContinentTitleQuery,
    ContinentTitleQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<
    ContinentTitleQuery,
    ContinentTitleQueryVariables
  >(ContinentTitleDocument, baseOptions);
}
export type ContinentTitleQueryHookResult = ReturnType<
  typeof useContinentTitleQuery
>;
export type ContinentTitleLazyQueryHookResult = ReturnType<
  typeof useContinentTitleLazyQuery
>;
export type ContinentTitleQueryResult = ApolloReactCommon.QueryResult<
  ContinentTitleQuery,
  ContinentTitleQueryVariables
>;
export const CountryTitleDocument = gql`
  query CountryTitle($id: String!) {
    countries(where: { id: { eq: $id } }, limit: 1) {
      name
    }
  }
`;

/**
 * __useCountryTitleQuery__
 *
 * To run a query within a React component, call `useCountryTitleQuery` and pass it any options that fit your needs.
 * When your component renders, `useCountryTitleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCountryTitleQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCountryTitleQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CountryTitleQuery,
    CountryTitleQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<
    CountryTitleQuery,
    CountryTitleQueryVariables
  >(CountryTitleDocument, baseOptions);
}
export function useCountryTitleLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CountryTitleQuery,
    CountryTitleQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<
    CountryTitleQuery,
    CountryTitleQueryVariables
  >(CountryTitleDocument, baseOptions);
}
export type CountryTitleQueryHookResult = ReturnType<
  typeof useCountryTitleQuery
>;
export type CountryTitleLazyQueryHookResult = ReturnType<
  typeof useCountryTitleLazyQuery
>;
export type CountryTitleQueryResult = ApolloReactCommon.QueryResult<
  CountryTitleQuery,
  CountryTitleQueryVariables
>;
export const CityTitleDocument = gql`
  query CityTitle($id: String!) {
    cities(where: { id: { eq: $id } }, limit: 1) {
      name
    }
  }
`;

/**
 * __useCityTitleQuery__
 *
 * To run a query within a React component, call `useCityTitleQuery` and pass it any options that fit your needs.
 * When your component renders, `useCityTitleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCityTitleQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCityTitleQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CityTitleQuery,
    CityTitleQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<CityTitleQuery, CityTitleQueryVariables>(
    CityTitleDocument,
    baseOptions,
  );
}
export function useCityTitleLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CityTitleQuery,
    CityTitleQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<CityTitleQuery, CityTitleQueryVariables>(
    CityTitleDocument,
    baseOptions,
  );
}
export type CityTitleQueryHookResult = ReturnType<typeof useCityTitleQuery>;
export type CityTitleLazyQueryHookResult = ReturnType<
  typeof useCityTitleLazyQuery
>;
export type CityTitleQueryResult = ApolloReactCommon.QueryResult<
  CityTitleQuery,
  CityTitleQueryVariables
>;
export const ContinentDocument = gql`
  query Continent($id: String!) {
    continents(where: { id: { eq: $id } }) {
      id
      name
      population
      countries {
        id
        name
        location {
          lat
          long
        }
      }
    }
    worldContinents: continents {
      population
    }
  }
`;

/**
 * __useContinentQuery__
 *
 * To run a query within a React component, call `useContinentQuery` and pass it any options that fit your needs.
 * When your component renders, `useContinentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useContinentQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useContinentQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    ContinentQuery,
    ContinentQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<ContinentQuery, ContinentQueryVariables>(
    ContinentDocument,
    baseOptions,
  );
}
export function useContinentLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    ContinentQuery,
    ContinentQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<ContinentQuery, ContinentQueryVariables>(
    ContinentDocument,
    baseOptions,
  );
}
export type ContinentQueryHookResult = ReturnType<typeof useContinentQuery>;
export type ContinentLazyQueryHookResult = ReturnType<
  typeof useContinentLazyQuery
>;
export type ContinentQueryResult = ApolloReactCommon.QueryResult<
  ContinentQuery,
  ContinentQueryVariables
>;
export const CountryDocument = gql`
  query Country($id: String!) {
    countries(where: { id: { eq: $id } }) {
      id
      name
      population
      currencies {
        name
        unitSymbols
      }
      languages {
        name
      }
      location {
        lat
        long
      }
      capital {
        id
        name
      }
      cities {
        id
        name
      }
      continent {
        name
        population
      }
    }
  }
`;

/**
 * __useCountryQuery__
 *
 * To run a query within a React component, call `useCountryQuery` and pass it any options that fit your needs.
 * When your component renders, `useCountryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCountryQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCountryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CountryQuery,
    CountryQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<CountryQuery, CountryQueryVariables>(
    CountryDocument,
    baseOptions,
  );
}
export function useCountryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CountryQuery,
    CountryQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<CountryQuery, CountryQueryVariables>(
    CountryDocument,
    baseOptions,
  );
}
export type CountryQueryHookResult = ReturnType<typeof useCountryQuery>;
export type CountryLazyQueryHookResult = ReturnType<typeof useCountryLazyQuery>;
export type CountryQueryResult = ApolloReactCommon.QueryResult<
  CountryQuery,
  CountryQueryVariables
>;
