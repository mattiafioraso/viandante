export type Coordinates = {
  readonly latitude: number;
  readonly longitude: number;
  readonly zoom: number;
};
