import React, {
  createContext,
  useState,
  Dispatch,
  SetStateAction,
  FunctionComponent,
} from 'react';

import { Coordinates } from './GeolocationProvider.model';

const defaultCoordinates = {
  latitude: 0,
  longitude: 0,
  zoom: 2,
};

export const GeolocationContext = createContext<
  readonly [Coordinates, Dispatch<SetStateAction<Coordinates>>]
>([defaultCoordinates, () => undefined]);

const GeolocationProviderProvider: FunctionComponent = ({ children }) => {
  const value = useState<Coordinates>(defaultCoordinates);

  return (
    <GeolocationContext.Provider value={value}>
      {children}
    </GeolocationContext.Provider>
  );
};

export default GeolocationProviderProvider;
