import React, { FunctionComponent } from 'react';

import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

const { REACT_APP_GRAPHQL_API_URL: uri } = process.env;

const apolloClient = new ApolloClient({
  uri,
  cache: new InMemoryCache(),
});

const GraphQLProvider: FunctionComponent = ({ children }) => (
  <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
);

export default GraphQLProvider;
