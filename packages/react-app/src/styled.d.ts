import 'styled-components';

type Palette = {
  readonly primary: string;
  readonly secondary: string;
};

declare module 'styled-components' {
  // eslint-disable-next-line functional/prefer-type-literal
  export interface DefaultTheme {
    readonly borderRadius: string;

    readonly palette: {
      readonly divider: Palette;
      readonly text: Palette;
      readonly main: Palette & { readonly contrast: string };
      readonly background: Palette;
    };
  }
}
